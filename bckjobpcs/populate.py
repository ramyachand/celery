import os 

import django
import random, time

os.environ['DJANGO_SETTINGS_MODULE']='bckjobpcs.settings'
django.setup()

from app.models import Job_Detail


# This is to insert random datas like amount and status by sleep time 1 second.
status = ['success','pending','failure']
while True:
	amt = random.randint(1000,2500)
	stat = random.choice(status)
	Job_Detail.objects.create(
		amount = amt,
		status = stat,
		)
	print(amt, stat)
	time.sleep(1)
		

