from django.db import models


#create model with amount and status field
class Job_Detail(models.Model):	
	amount = models.IntegerField(verbose_name = "Amount")
	status = models.CharField(verbose_name = "Status", max_length = 50)
