from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect

from app.models import Job_Detail
from app.tasks import manual_func


def cel_task(request, templates="cele.html"):
	'''
		count success,pending and failure data to pass the data
		to html through args.
		if status is pending call the celery task to change the status.
	'''
	args = {}
	sus_count = Job_Detail.objects.filter(status='success').count()
	pen_count = Job_Detail.objects.filter(status='pending').count()
	fai_count = Job_Detail.objects.filter(status='failure').count()

	args = {
	"sus_count":sus_count,
	"pen_count":pen_count,
	"fai_count":fai_count
	}

	if request.POST.get('pending_job'):
		manual_func.delay()
		return HttpResponseRedirect('/celtask/')
	return render(request, templates, args)  
