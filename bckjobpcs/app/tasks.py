import random
from celery import Celery
from celery.task.schedules import crontab
from celery.decorators import periodic_task,task

from app.models import Job_Detail

app = Celery()

# create your celery task
@task(name="manual_func")
def manual_func():
	print("Manual Job Started")
	return update_pending_txn()

#create task which will run at scheduled time
@periodic_task(run_every=(crontab(minute='*/5')), name="periodic_func", ignore_result=True)
def periodic_func():
	print('Time Interval Job Started')
	return update_pending_txn()

#update status from pending to either as success or failure by user
def update_pending_txn():
	status = ['success', 'failure']
	stat = random.choice(status)
	tnx = Job_Detail.objects.filter(status='pending').update(status=stat)
	return "%s pending txn updated" % tnx

