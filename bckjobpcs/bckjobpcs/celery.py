import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bckjobpcs.settings')

app = Celery('bckjobpcs')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()